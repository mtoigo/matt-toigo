---
title: Pulling Google Calendar Events with Node.js
summary: A quick tutorial on pulling down Google Calendar data with Node.js
published: 2014-12-07
tag: dev
layout: default.hbs
---

	<p>At my office the competition for conference rooms has gotten fierce. The importance of a meeting can vary greatly so it's good to know what's going on in a room before you interrupt someone inside to see if you can steal it. There's always the option of opening a laptop to check the schedule for a room, but we were looking for a more slick solution. The idea of mounting iPads near each conference room that explained the details on the current meeting was born. This had previously been investigated, but was fairly difficult when using Outlook for scheduling. Luckily we had just switched to Google Apps for our organization so we gave it another shot. Each room has it's own Google Calendar that is viewable by anyone in our organization which makes this possible.</p>

<img src="../images/google_calendar_node_tutorial/ipad.jpg" alt="iPad" class="screenshot">

<p>The goal was to have a web page that would display the owner, title, and duration of the current meeting, or if the room was open, what time was free until. The finished product is a combination of Node, Express, and Angular. This tutorial will focus just on the bit pulling the events themselves since it's the most tricky. I usually use PHP for larger projects, but Node is great for these kind of fun small one off projects.</p>

<p><strong>This tutorial assumes you have <a href="http://nodejs.org/">Node.js</a> installed and a basic familiarity with it as well as the command line. You should also already have a Google account.</strong></p>

<h2>Setup</h2>

<p>Starting in an empty directory, create the file <em>package.json</em> with the following contents</p>
<script src="https://gist-it.appspot.com/github.com/mtoigo/NodeGoogleCalendarTutorial/blob/master/package.json"></script>

<p>You can then run the following command to install the three required node modules.</p>

<div class="terminal">npm install</div>

<p>You should now have the express, googleapis, and moment node modules installed in a local folder named node_modules. The module versions are the most recent at the time of this tutorial, but you should try using * for the version number to get the latest and greatest to see if that works first.</p>

<h2>Google Configuration</h2>
<p>Head over to <a href="https://console.developers.google.com/">Google Developer Console</a> and click <strong>Create Project</strong>. The project may take a minute to fire up. Afterwards click <strong>APIs & auth</strong> over the left and then <strong>APIs</strong>. Look for <strong>Google Calendar</strong> and then turn it on.</p>

<p>Over on the left click <strong>Credentials</strong> under <strong>APIs & auth</strong>. Click <strong>Create new Client ID</strong> under OAuth. Choose <strong>Web Application</strong>. Fill out <strong>Product Name</strong> and <strong>Email</strong> on the consent screen. On the <strong>Create Client ID</strong> screen use the following values and then click <strong>Create Client ID</strong></p>

<img src="../images/google_calendar_node_tutorial/google_config.png" alt="Google Config" class="screenshot">

<p>After everything is setup you should see the following.</p>

<img src="../images/google_calendar_node_tutorial/oath_credentials.png" alt="Google Config" class="screenshot">

<p>Save the values for CLIENT ID and CLIENT SECRET. You'll need them in a later step.</p>

<h2>Google Calendar ID</h2>
<p>You'll need the ID of the calendar that you want to pull events for. The easiest way is to find the calendar you want, and then look for <strong>Calendar settings</strong>. Down the screen you should see <strong>Calendar ID</strong> which will either look like <strong>username@gmail.com</strong> or something like <strong>sjb1qhbicuu3u0kl6suat53h8c@group.calendar.google.com</strong></p>

<img src="../images/google_calendar_node_tutorial/calendar_settings.png" alt="Calendar Settings" class="screenshot">

<h2>Code</h2>

<p>Place the following source code in a file named <em>app.js</em></p>
<script src="https://gist-it.appspot.com/github.com/mtoigo/NodeGoogleCalendarTutorial/blob/master/app.js"></script>

<p>You'll need to edit the following lines (3:5) with the credentials you saved from the Google API Console and also your Calendar ID.</p>
<script src="https://gist-it.appspot.com/github.com/mtoigo/NodeGoogleCalendarTutorial/blob/master/app.js?slice=2:4&amp;footer=0"></script>

<p>Save the file and at this point you should be all set. Run the following command and then visit <a href="http://localhost:2002/">http://localhost:2002</a> in your browser.</p>

<div class="terminal">node app.js</div>

<p>You should be redirected to a Google OAuth page where you can grant your app permission to read your calendar. If you have multiple Google accounts, be sure to choose the one that has access to the calendar you want to display. After accepting you should be bounced back to http://localhost:2002 where you should see JSON output for the events on the calendar you specified. Make sure you have events on your calendar for the current day if you don't see anything and then refresh the page.</p>

<img src="../images/google_calendar_node_tutorial/sample_json.png" alt="Sample JSON" class="screenshot">
<div class="caption">I have the <a href="https://chrome.google.com/webstore/detail/jsonview/chklaanhfefbnpoihckbnefhakgolnmc?hl=en">JSONView</a> Chrome Extension installed which makes the output easier to read.</div>

<h2>Next Steps</h2>
<p>Now that you have the data you need, you can write frontend Javascript to process this JSON at a set frequency and iterate through the events to display which returned event coincides with the current time. Looking through the JSON above you can see that I had a bike race all day today.</p>

<h2>Notes</h2>
<ul>
<li>The entire source for this application is available at: <a href="https://github.com/mtoigo/NodeGoogleCalendarTutorial">https://github.com/mtoigo/NodeGoogleCalendarTutorial</a></li>
<li>OAuth can be a little tricky to understand at first so you should read through Google's implementation <a href="https://developers.google.com/accounts/docs/OAuth2">here</a> and also the <a href="https://developers.google.com/gdata/faq#AuthScopes">related scopes</a>.
<li>There are better ways to architect this type of application, but this is designed to be as concise as possible.</li>
<li>IMPORTANT: Do not run this at a public URL if the calendar you are showing is private. This would mean you are showing private events at an open public URL.</li>
<li>If it's possible, it's best to setup a generic google user to pull these calendars that has read only access to them rather than your own personal Google account.</li>
<li>This implementation persists the OAuth refresh token in memory which is not ideal. You only get a refresh token when you initially authorize an application. If you do not have a proper refresh token, requests to Google will fail after an hour. The workaround for this is to <a href="https://security.google.com/settings/u/1/security/permissions">revoke your application's access</a> from your Google account whenever you start it or to persist the token information in a data store.</li>
<li>One gotcha is that cancelled events will still come back so be sure to check the status field to make sure events are still valid.</li>
</ul>
