---
title: Performance Issue Conversations with Software Engineers
summary: How to talk to your teams when something isn't working.
published: 2024-01-17
tag: dev
image: /photos/performance/performance.jpg
layout: default.hbs
---

<img src="/photos/performance/performance.jpg" alt="Two People Talking" class="inline">

<p>
A part of every Engineering Manager's job at some point will be a conversation with a direct report about them not getting done what they're expected to. If you're reading this and haven't had to do this yet, eventually you will need to. The word <strong>NEED</strong> is important here because if you're hiring great people, they're often kind and can absorb other teammates' problems leading to one person eventually dragging down an entire team. It's a situation that's not fair to the team.  As an Engineering Manager, it's your responsibility to solve the performance issue and provide a good operating environment for your overall team. If you're a Director/VP/CTO reading this, it's your job to help Managers through these conversations and especially so if it's the first time they've had to work through something like this.
</p>

<h1>Approaching the Conversation</h1>
<p>
There are various steps in this process, but performance issues are not difficult to spot if you're doing skip levels and have basic work tracking in issue software like JIRA to watch for work not getting finished. One of the most important steps in this process is having a conversation with an employee to go over the specific examples of work not meeting the expected standards and <strong>having them acknowledge that something isn't working well.</strong> If someone is completely in denial or only making excuses then there is a very low likelihood of resolving the underlying cause. You should also stress that you're talking to them about this because you believe in them and that they can do better and you want to help them.
</p>
<p>
Once someone acknowledges that something isn't working the way it should, <strong>then you are through the most difficult part of the conversation</strong> and it will usually get easier from there. The next correct tactic is to approach this as <em>"I'm your manager and it's my job to help you, so let's chat about what's going on and how I can best support you”.</em> This approach can sound very open ended so this step goes much better if you have a list of possible causes that you can run through and you can rule them out together. <strong>The list below is what I give Managers the first time they need to have this conversation.</strong> There could be other causes, but this list is pretty comprehensive assuming you're not working in a completely dysfunctional workplace. If that's the case then you'll usually see performance issues across entire teams instead of with just one person.
</p>

<h1>The Causes to Talk Through</h1>
<p>You should go over each of these to rule them out with your employee after they acknowledge something is going wrong.</p>
<h2>1. Too Much Work / Overwhelmed</h2>
<p>
You can spot this one multiple ways but bring up the active work on someone's plate, review it with them, and ask if it's too much. Do they have more than 3 engineering tasks in progress at a time? Are they constantly jumping around from one task to another and never finishing anything? Do they feel like they're always behind? If they do or you sense it, <strong>offer to take one or two tasks off their plate to just take some pressure off so they only have one task to complete at a time.</strong> After a few sprints, evaluate if the decreased workload resulted in higher quality work. There are people who often overcommit and give feedback here that trying to do everything will usually mean doing nothing. You should also encourage your teams to push back during sprint planning and make sure to attend if this is a pattern where they're consistently committing to more work than they can handle.
</p>

<h2>2. Unclear Prioritization / Shifting Priorities / Lacking the Big Picture</h2>
<p>
Engineers are great at working through punch lists of problems to solve, but if that list is constantly changing or they don't understand the big picture, nothing will get done. It's extremely important for both Engineering Managers and Product Owners to give their teams stability and a clear vision of where they're headed. Engineers should understand why some tasks are more important than others (not just that they are). They should have stability in a sprint and the opportunity to influence work before a sprint starts, and they should understand how all of the pieces of work fit together. This could be an issue with an engineer <strong>missing critical information so make sure they know where to find this information</strong> (from Confluence, Meetings, Presentation, JIRA, etc.). There is also <strong>no substitute for a Product Owner and Engineering Manager just spending time talking to a team about the overall picture of what's being built.</strong> There are also multiple other benefits here such as engineers making better decisions as they build features because they understand the overall goal.
</p>

<h2>3. Personal Issues</h2>
<p>
We're human beings, not robots that move JIRA tickets from left to right endlessly. People will have good days and bad days. Good weeks and bad weeks. This one can be trickier for a manager to chat through. My approach has always been the following:
</p>

<ul>
<li><em>"You never have to tell me (your manager) about things in your personal life if you don't want to. I'll never ask, but if something is affecting your long-term performance then you need to work to solve it."</em>
    <ul><li><strong>The word long-term here is important.</strong> Look at how someone performs over quarters and don't focus on a single bad day or week.</li></ul>
</li>
<li><em>"I'm here if you want to chat about anything. I've worked through lots of stuff myself (sometimes with the help of others) and can hopefully give you advice, but that's not required and choosing to keep personal issues private is not something that affects my evaluation of how you do your job."</em></li>
</ul>
<p>
People vary drastically with this one. Some people want their non-work lives to remain private, and some people want someone to talk to. I think it's a manager's job to offer that support and option. There is no correct answer here and a manager should absolutely never pry for information or demand an employee tell them what's going on.
</p>

<h2>4. Not Interested in The Work / Burnout</h2>
<p>
You can usually sense disinterest and burnout, and in this case someone won't have a great attitude about the work in general. You should just ask directly, <em>"Do you enjoy the work? Do you enjoy the mission? Are you burned out?”</em> Offer that it's ok and it happens sometimes. A manager's job is walking the line between what a company needs done and what an employee wants to do in terms of their career. These are not always 100% in sync. Chat through exactly what they don't like doing at the moment, what they want to do instead, and give yourself a task to help find ways to make the work more interesting and less repetitive for them. <strong>Internal transfers to other teams are also a great option here</strong> and there are often people in other parts of your organization that also want a change.
</p>

<h2>5. Not Enough Time to Work / Too Many Meetings</h2>
<p>
This problem has worsened since the pandemic. Hopefully you can see someone's calendar by sending them a meeting invite and understand how much non-meeting time they have. Meeting commitments will vary, but if you have an individual contributor (non manager) engineer who's consistently in more than 3 hours of meetings a day, that's a major problem. Most engineers should ideally have less than that, but can usually be about that amount for lead engineers. Offer to look through their calendar and go over the following:
</p>
<ul>
<li>What meetings are they in that you can cover for them?</li>
<li>What meetings do they go to that they feel they don't really contribute to?</li>
<li>Let them know that they can skip most meetings you invite them to, just make sure to let you know beforehand and you'll inform them if there's anything critical.</li>
<li>Encourage your teams and wider organization to use (optional) if it exists in your scheduling software and that it really means OPTIONAL. Google Calendar has this and it's a great way to let people know something is happening, but they don't necessarily need to attend.</li>
<li>Identify specific patterns where other teams may over-invite engineers and work with them to invite Engineering Managers or Product Owners instead.</li>
</ul>

<h2>6. Lacking Support from Teammates</h2>
<p>
Software engineering is difficult and it looks different at almost every organization. Both in processes, coding patterns, and technical stacks. One of the most important behaviors and signs of a healthy team is one that dedicates time to onboarding new teammates and helping each other. You should directly ask if someone feels supported by the rest of the team and always ask this occasionally during their first 6 months when this support is critically necessary. If they don't feel supported, directly ask teammates to give them more support, but also know there are limits here. If someone doesn't start to gain traction after 6 months and begin to work independently, there could be other issues. One of the best ways to set this expectation with your teams is for <strong>you yourself to send a strong signal on the importance of onboarding and support</strong>. Make sure that you spend time with new hires (even if you're in a higher level position like a Director/VP/CTO) so that managers see how important this is for them to do with their direct reports.
</p>

<h1>The Cause You Can't Talk Through</h1>
<p>
After you've talked through and ruled out the 6 possible causes above, there may be others, but the most likely cause is someone not having the correct skills for a position. There are skill gaps that can be resolved through training and mentoring, but there are also situations where a candidate just isn't the right fit for a role. <strong>This is not something you can usually ask someone directly.</strong> You need to rely on conversations with their teammates, pairing with them to work on tickets, and asking them to talk through their approaches to problems. You can use this to gauge if they really don't possess the technical, communication, or organizational skills for the current level of their position. These situations often unfortunately can't be resolved and the options are then to see if there's anything within the organization that better suits them and if not begin the process of letting them go.
</p>

<h2>Did We Mess Up and Hire the Wrong Person?</h2>
<p>
Now you'll likely ask yourself <em>"How did we hire the wrong person? What did we miss in our interview process?"</em> and the answer is that it happens sometimes and is never completely preventable. The reason why this will happen sometimes is because you <strong>should only hire people that you're around 80% to 90% confident are the right fit for a role.</strong> Why not only hire someone you're sure is 100% the right fit? The answer there is that you'll likely miss out on the best candidates for multiple reasons and this is especially true if the skills you're hiring for are in demand. There are companies that take months to make a candidate a job offer while trying to make sure they're 100% correct for a role. If that candidate is truly exceptional there are other companies who want them too and some of those companies can <strong>run a full interview process in 2 weeks flat from intro call to offer.</strong> Also, interviews are run by humans and humans are imperfect, even with standardized interview approaches. We need to accept that no interview process can be flawless and sometimes the result may not end up being what we expected.
</p>
<p>
<strong>Performance conversations are sometimes painful and the best approach is to make sure you give someone plenty of space to speak, genuinely listen to them, and then be direct, honest, and consistent in what you need to tell them.</strong>
</p>
<p>An extra note here as well that a great book to help with these situations is <a href="https://www.amazon.com/Difficult-Conversations-Discuss-What-Matters/dp/0143118447">Difficult Conversations</a> which explores the idea that any conversation which can potentially change someone's self-perception or identity is inherently difficult. Once you understand exactly what makes a conversation difficult, it's often easier to have.</p>