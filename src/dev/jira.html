---
title: The Big 3 Jira Mistakes
summary: Software engineers spend hundreds of hours a year in Atlassian Jira and these are the 3 big mistakes organizations often make with it.
published: 2024-09-30
tag: dev
image: /photos/jira/jira.png
no-shadow: true
layout: default.hbs
---

<img src="/photos/jira/jira1.svg" alt="Jira Workflow" class="inline" style="margin-top: 20px">

<p>
Every software development methodology requires you to track the completion of tasks. This is the fundamental truth on which most software development project management and issue (ticket) tracking software is based. There are nearly infinite methodologies, tactics, and tools which can be combined with issue tracking software to suit the particular needs of both your organization and your project.
</p><p>
I’ve worked in established product companies, startups, and consulting. <strong>The development pattern I see most often is using <a href="https://www.atlassian.com/software/jira">Atlassian Jira</a> to run 2 weeks sprints with some version of Agile and Scrum.</strong> The concepts of Agile and Scrum and also Jira itself can easily be criticized, but they are tools like everything else in the software industry and they can be tailored and applied very well to situations or extremely poorly.
</p><p>
I’ve seen organizations where Jira was universally hated and others where it was seen as useful at all levels of product and engineering. Issue tracking software is something that teams spend hours every day looking at so it's always going to elicit very strong opinions. Like most successful adoptions of software, using Jira effectively comes down to listening to what people need, making good decisions based on what the software can actually do, and establishing light standards and governance balanced with a process for anyone to suggest new changes.
</p><p>
The above sounds like good standard generic boring software practices so what is far more interesting are these 3 mistakes that I've repeatedly seen organizations make when using Jira.
</p>
<ol>
<li>Every Team Can Have Their Own Workflow</li>
<li>Overly Rigid Workflows</li>
<li>Sprint Points and Velocity Are the Only Metric that Matters</li>
</ol>

<h1>1. Every Team Can Have Their Own Workflow</h1>
<p>
A key concept in Jira is that any given issue (Story, Bug, Task, etc.) must have a single status assigned to it. This status usually describes where the work currently is in the software development process and some common examples are:
</p>

<ul>
    <li>Unprioritized</li>
    <li>To Do</li>
    <li>In Progress</li>
    <li>Rejected</li>
    <li>Ready for QA</li>
    <li>In QA</li>
    <li>Needs Product Approval</li>
    <li>Done</li>
</ul>

<p>
If you've ever been part of a software development team, you'll look at the above list and think that you've seen some of these before and they mostly make sense. You may have questions about a few of them and your own teams' interpretation of these may vary, but you could make a reasonable guess at what they mean. As varied as software development processes can be, about 90% of teams follow roughly similar workflows and some of these steps may also be skipped if they're not necessary to how your team develops software.
</p><p>
In Jira the process of defining these statuses that can be assigned to an issue is called a <a href="https://www.atlassian.com/software/jira/guides/workflows/overview#what-is-a-jira-workflow">Workflow</a>. Organizations that manage Jira poorly make a non-decision and let every team define their own Workflow which seems ok at first but becomes a massive headache as you scale to managing anything more than 10 teams. When you hit 100 teams, you then have 100 different Workflows to manage in Jira.
</p><p>
So what's wrong with letting each team make their own Workflow? There are 3 major problems that will surface:
</p>

<h2>Lacking Common Terminology for Teams to Communicate</h2>
<p>
It's inevitable that engineers will shift teams, one team will help another out, or that multiple teams will work on a major initiative together. Remote teams (and in-person teams) require strong communication, so aligning the terms that we use to describe the status of work will create a common language early on. There is nothing gained by each team using slightly different words to describe an issue that is In Progress rather than all just agreeing on what In Progress means.
</p>

<h2>Difficult to Create Filters + Dashboards Across Multiple Teams</h2>
<p>
One of the most powerful features in Jira is the ability to gain high level views of how development is progressing using JQL (Jira Query Language), Queries, and Dashboards to analyze patterns with work being performed across multiple teams. Most Engineering leaders use these features to create dashboards and a common one would be to show all work in testing. If everyone is using the same workflow this could be something simple such as:
</p>

<p>A simple JQL query when 3 projects have the same Workflow</p>
<p class="code">project in (JNR,RUY,BWA) AND status = "In Testing" ORDER BY resolved</p>

<p>
But what if each team is using a custom Workflow + statuses to denote work in testing? The above query would need to be aware of all the different testing work statuses and would not work correctly as soon as a team changed the name of their individual status that represented work in testing.</p>

<p>A complex query when 3 projects have different workflows.</p>
<p class="code">(project = JNR AND status = "In Testing") AND (project = RUY AND status = "QA") AND (project = BWA AND status = "Testing") ORDER BY resolved</p>

<p>This is partially solved using <a href="https://support.atlassian.com/jira-work-management/docs/workflows-and-statuses-for-the-board/">Jira Status Categories</a>, but these only allow you to group workflow statuses by To do, In Progress, and Done so there will still be issues with more fine grained filtering on other common statuses.</p>

<h2>Slower to Move Issues Between Teams</h2>
<p>
Jira issues are free to write and there's almost never a good reason to not write an issue. If you don't create an issue when you see a problem, a critical bug may be missed. If someone already documented a bug, they can just close your new issue as a duplicate. As Jira issues are created, it's inevitable that some will be made in the incorrect projects under the wrong teams who can't solve the issues. This is normal and expected. Jira easily lets you move an issue between projects, but this becomes more complex if you have to also convert from one status to a different status because the same Workflow isn't shared between both projects. It adds an extra step where you will need to go talk to a team to understand what their individual Workflow statuses mean and how they map from the project where the issue currently is to the new project where an issue is being moved to.
</p>

<h2>A Nightmare for IT</h2>
<p>
Atlassian does not have a hard limit on the number of Workflows that one organization can create, however your Jira instance does have an Administrator and they're the one who will help out when things go wrong. They have a lot to manage, and troubleshooting issues for teams is much easier if those teams are all on the same Workflow that is well known and understood. If each team has created unique over-complicated custom Workflows, they'll need to invest time in understanding how they work before they can find out what's going wrong.
</p>

<h1>2. Overly Rigid Workflows</h1>
<p>
Now that we understand why each team having their own Workflow can create more problems than it solves, there's another very common mistake organizations make related to Workflows.
</p><p>
In Jira you can create very strict rules which govern how and when an issue can change from one status to another. These rules are often created with good intentions (see Mistake #1!) and an example rule might be "an issue must only move to the <em>In QA</em> status if it was previously in the <em>In Progress</em> status". For a user, this means they can't easily click and drag an issue from To Do to In QA even if that's the most logical thing to do. They now need to take an extra meaningless step to set it to In Progress and then move it to In QA. This single example may not seem too burdensome, but when an organization has a lot of these types of Workflow restrictions they decrease the productivity of teams.
<p></p>
When this happens you'll see teams frequently complain about having to trick Jira into doing what they want, not bothering to update statuses correctly since it's too much work, and adding in meaningless values to required fields.  You may also see teams move software development tracking out of Jira entirely into Trello, Asana, or spreadsheets which have no workflow rules. I have consulted with multiple organizations where Jira was strongly disliked and this was often the major reason why. <strong>It was viewed not as a tool to help develop software, but rather as a poorly understood process that had to be followed strictly in parallel to the actual work of developing software.</strong>
<p></p>
There are some environments where compliance and auditing are extremely critical and these Workflow rules are established with the intent of ensuring software engineers follow the correct processes. These rules should absolutely exist in these types of environments, but if you're not working a highly regulated environment you should consider two important points:
<p></p>

<ol>
    <li>Actions taken in a piece of software don't actually ensure processes are being followed. It only ensures users set an issue to certain statuses at certain times, but they may not even be aware of what those statuses mean, why the processes are important, and what the processes are meant to deliver.</li>
    <li>Jira retains easily viewable history on every issue. For auditing you can always look at every single change to an issue, who made it, when, and what statuses it was previously set to. You can also always look back at this history and quickly correct any mistakes.</li>
</ol>

<h2>A Common Restrictive Jira Workflow</h2>
<img src="/photos/jira/jira1.svg" alt="Jira Workflow" class="inline" style="margin-top: 20px">

<h2>What Do Better Workflows Look Like?</h2>
<p>
A better workflow can vary somewhat depending on the type of software engineering being performed and the organizations' needs. I highly encourage most teams using Jira to adopt a Workflow where any status can be transitioned to any other status without any screens or required fields.
</p>
<p>
In practice, this means a team can move an issue between columns on a board however they need to. If something needs to go back to In Progress from any point in the development workflow, they can make that change in seconds by dragging and dropping the issue without navigating additional required rules, steps, or mandatory fields.
</p>
<img src="/photos/jira/jira2.svg" alt="Jira Workflow" class="inline" style="margin-top: 20px">
<p>
This may sound like chaos at first, but if you have reasonably sized teams  (5 engineers or less), everyone should be aware of all the work on the board, review it daily, and commit to keeping the board up to date. Teams should always have clarity on the process to follow at each step of developing software and establishing a team charter can help clarify that process. A few example charter rules your team might consider include: 
</p>

<ul>
    <li>All work needs to be assigned points before it comes into a sprint.</li>
    <li>An engineer should sit with a tester to explain work to them before QA begins.</li>
    <li>A designer should review work before it's launched.</li>
    <li>A product owner should verify work as complete on production and close an issue.</li>
</ul>

<p>
It may feel correct to try to add all of these rules into a Jira Workflow to ensure they're being followed, but in my experience these rules become a complicated mess to navigate and are strongly disliked by the entire team. <strong>Trust, talking about process alignment, reviewing work + process later, and over-communication in a correctly sized team is always the better solution rather than trying to enforce strict rules in a Jira Workflow.</strong>
</p>

<h1>3. Sprint Points and Velocity Are the Only Metric that Matters</h1>
<p>
Jira generates a wealth of data about the process of developing software. It's common for teams to assign points such as 1,2,3,5,8,13 to each issue that they work on. Historically in Scrum these points represent the product value added to the software, but in practice teams usually use them as a mix of describing effort + time + complexity to ensure that they are only taking a reasonable amount of well understood development work into a sprint.
</p>
<p>
The sum of the points on the issues that a team completes in a 2-week sprint are commonly known as their velocity. After enough sprints a team can usually get comfortable with how much work they can commit to completing in new sprints assuming there's no major changes to their workstream. In my experience managing 3 to 5-engineer teams working on web applications, velocity can generally vary from 20 points to 70 points in a 2-week sprint.
</p>
<p>
An example of a project with lower velocity was a 3-engineer team working on an incredibly complex backend billing system that could not have any bugs and was difficult to test. An example of a higher velocity team would be a 5-engineer team that is converting Figma files to frontend, HTML, and CSS. The nature of this work is completely different and both teams may think differently about what a 3 point issue means so comparing the velocity of the two teams would be meaningless.
</p>
<p>
Velocity data exists in Jira which allows you to compare the amount of points that teams and individuals are completing, but it <strong>should generally not be used as a measure of performance to compare teams</strong>. It is very easy to pull reports that rank individuals and teams according to the number of points they've completed but this generally never ends well.
</p>
<p>
Some teams may require much more customer and product interaction to spec features which takes longer. Other teams may have much more complex QA cycles. Some engineers may need to spend more time assisting product owners. Some engineers may spend more time mentoring and ramping up junior engineers. <strong>Sending a signal to teams or individuals that the only way you will evaluate their performance is by the number of points resolved per sprint always ends up in extremely toxic teams.</strong> Performance of teams should be measured by the their ability to meet product goals and deadlines that they have had a hand in establishing themselves.
</p>
<p>
So why are we tracking points and velocity then? It is absolutely fine and healthy for teams to use their velocity for their own planning and estimation purposes. Points and velocity are not perfect, but they are generally the best tool we have that allows a team to understand the complexity of work, describe it, and help establish reasonable estimates to inform deadlines.
</p>
<h1>Change is Constant</h1>
<p>
Hopefully now that you're aware of these 3 common Jira mistakes you can avoid them.</p><p>Another very critical piece to making Jira successful is a process where anyone can suggest changes and the suggestions will receive a fair evaluation. This process should ensure the individual who suggested the change is given a chance to explain why it's valuable and gather input from other teams. Creating a culture where anyone can suggest process improvements from anywhere in the organization is a general good practice in any industry. It is extremely important though in this case because Jira is such a large part of most developers' days that even small changes can have massive impacts on job satisfaction, software quality, and the time it takes to ship new features.
</p>