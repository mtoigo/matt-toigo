/* METALSMITH */

const Metalsmith  = require('metalsmith'),
  collections = require('@metalsmith/collections'),
  layouts = require('@metalsmith/layouts'),
  templates = require('metalsmith-templates'),
  Handlebars = require('handlebars'),
  hbtmd = require('metalsmith-hbt-md'),
  dateFormatter = require('metalsmith-date-formatter'),
  canonical = require('metalsmith-canonical');

Handlebars.registerHelper('if_eq', function(a, b, opts) {
  if (a == b) {
    return opts.fn(this);
  } else {
    return opts.inverse(this);
  }
});

Metalsmith(__dirname)
  .source('./src')
  .destination('./build')
  .clean(true)
  .use(templates('handlebars'))
  .use(
    collections({
      dev: {
        pattern: 'dev/*.html',
        sortBy: 'published',
        reverse: true
      },
      wheels: {
        pattern: 'wheels/*.html',
        sortBy: 'published',
        reverse: true
      },
      build: {
        pattern: 'build/*.html',
        sortBy: 'published',
        reverse: true
      },
      travel: {
        pattern: 'travel/*.html',
        sortBy: 'published',
        reverse: true
      },
      all: {
        pattern: '{dev,wheels,travel,build}/*.html',
        sortBy: 'published',
        reverse: true
      }
    })
  )
  .use(dateFormatter({
    dates: [
      {
        key: 'published',
        format: 'MMMM Do, YYYY'
      }
    ]
  }))
  .use(hbtmd(Handlebars, {
    pattern: '*'
  }))
  .use(canonical({
    "hostname": "https://matt-toigo.com",
    "omitIndex": true,
    "omitExtensions": ['.html']
  }))
  .use(layouts())
  .build(function(err) {
    if (err) throw err;
  });
