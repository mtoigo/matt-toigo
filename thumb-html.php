<?php
// Image sizes should be 150 wide and 700 wide
// Generate HTML for thumbnails given a directory full of image names
// php thumb-html.php /Users/matttoigo/dev/matt-toigo/src/photos/f85x/big
// https://imageoptim.com/howto.html

$dir = $argv[1];
$files = $files1 = scandir($dir);

$parts = explode('/', $dir);
$lastDir = $parts[count($parts) -2];

echo('<section class="thumbnails">'."\n");
foreach($files as $file) {
  if($file != '.' && $file != '..') {
    echo('<a rel="lightbox[set]" href="/photos/'.$lastDir.'/big/'.$file.'">'."\n");
    echo('<img src="/photos/'.$lastDir.'/small/'.$file.'" data-thumb="/photos/'.$lastDir.'/small/'.$file.'">'."\n");
    echo('</a>'."\n");
  }
}
echo('</section>'."\n");
 ?>
