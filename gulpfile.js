/* GULP */
const gulp = require('gulp'),
  sass = require('gulp-sass')(require('sass')),
  cleanCSS = require('gulp-clean-css');

gulp.task('default', () => {
  return gulp.src('src/sass/**/*.scss')
      .pipe(sass().on('error', sass.logError))
      .pipe(cleanCSS())
      .pipe(gulp.dest('build/css/'));
});