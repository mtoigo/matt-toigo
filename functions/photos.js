const {OAuth2Client} = require('google-auth-library');

// Download your OAuth2 configuration from the Google
const keys = {
  "web": {
     "client_id": "1015818130125-u2lrddccu80cain7ni102q08tor3qi3h.apps.googleusercontent.com",
     "project_id": "matt-toigo",
     "auth_uri": "https://accounts.google.com/o/oauth2/auth",
     "token_uri": "https://oauth2.googleapis.com/token",
     "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
     "client_secret": "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",
     "redirect_uris": [
        "https://matt-toigo.com/.netlify/functions/photos"
     ],
     "javascript_origins": [
        "https://matt-toigo.com"
     ]
  }
};

// which should be downloaded from the Google Developers Console.
const oAuth2Client = new OAuth2Client(
  keys.web.client_id,
  keys.web.client_secret,
  keys.web.redirect_uris[0]
);

/*
  // May need this to check expiring tokens and do something with them
  // After acquiring an access_token, you may want to check on the audience, expiration,
  // or original scopes requested.  You can do that with the `getTokenInfo` method.
  const tokenInfo = await oAuth2Client.getTokenInfo(
    oAuth2Client.credentials.access_token
  );
  console.log(tokenInfo);
}*/

function generateAuthURL() {

    // Generate the url that will be used for the consent dialog.
    const authorizeUrl = oAuth2Client.generateAuthUrl({
      access_type: 'offline',
      scope: 'https://www.googleapis.com/auth/photoslibrary.readonly',
      prompt: 'consent'
    });

    return authorizeUrl;
}


async function getAuthenticatedClient(code) {

      const r = await oAuth2Client.getToken(code);
      // Likely just take the tokens and put them in ENV variables
      oAuth2Client.setCredentials(r.tokens);

      console.log('oAuth Tokens');
      console.log(r.tokens);

      return oAuth2Client;
//  });
}

exports.handler = async function (event, context) {

  const credentials = JSON.parse(process.env.OATH_CREDENTIALS);
  oAuth2Client.setCredentials(credentials);

  const code = event.queryStringParameters.code;
  if (code) {

    const oAuth2Client = await getAuthenticatedClient(code);

    return {
      statusCode: 200,
      body: 'Back from oAuth. Get tokens from the logs and set environment variable.'
    };
  }
  else if(oAuth2Client.credentials.access_token) {

    // https://github.com/JustinBeckwith/gaxios
    const requestDetails = {
      url: 'https://photoslibrary.googleapis.com/v1/mediaItems:search',
      method: 'POST',
      data: {
        albumId: "AE99AWT5zEwRBr1vEaRVesPPwYpa11OzpHYgbCXJd_uVP2Xep58Ylxd1wa5rPx4ruUJj12-uwKTJ",
        pageSize: 100
      },
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + oAuth2Client.credentials.access_token
      }
    };
    
    const response = await oAuth2Client.request(requestDetails);
    const items = response.data.mediaItems;

    // Google photos not returning items in the order is should
    items.sort((a,b) => (a.mediaMetadata.creationTime < b.mediaMetadata.creationTime) ? 1 : ((b.mediaMetadata.creationTime < a.mediaMetadata.creationTime) ? -1 : 0));
    const body = items;

    // Can't cache response, google images start failing eventually, google must be changing image URLs so we can't catche
    return {
      statusCode: 200,
      body: JSON.stringify(body),
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/json'
      }
    };
  } else {

    // START HERE - Issue is netlify doesn't persist the oAuth2Client between requests, likely need to store it somewhere
    console.log(oAuth2Client);

    return {
      statusCode: 200,
      body: '<html><a href="' + generateAuthURL() + '">oAuth Flow</a>'
    };
  }
};
